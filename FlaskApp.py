from flask import Flask, render_template, jsonify, request
import extract
from planar_maps import planarMap
from tree import Tree
from ternaryTrees import getAllTreeReps
import process
import os
from sys import platform
app = Flask(__name__)
   
@app.route("/")
@app.route("/input")
def input_new():
	treeArgs = request.args.get('Tree')
	args = []
	treeDict = {1: [], 2: [], 3: []}
	ddpDict = None
	if treeArgs is not None:
		treeArgs = treeArgs.replace(' ', '')
		args = [treeArgs[i:i+4] for i in range(0, len(treeArgs), 4)]

		if platform == "linux" or platform == "linux2":
			prog = "skew.out"
		elif platform == "win32":
			prog = "skew.exe"
		else:
			return render_template("error.html", error="no suitable os!")

		program = os.path.join(os.path.dirname(__file__), prog)
		output, error = process.runProccess(program, args)

		if len(output) > 0:
			ddpDict, treeDict = extract.extractProcessOutput(output)

		if len(error) > 0:
			return render_template("error.html", error=' '.join(error))

	trees = []

	trees.append(Tree(args, x_shift = 0.5).getData())

	n = 'T'
	for tree in treeDict:
		n += '+'
		trees.append(Tree(treeDict[tree], x_shift = 0.5, name=n).getData())

	graphs = []
	if ddpDict is not None:

		for ddp in ddpDict:
			graphs.append(getGraph(ddpDict[ddp]))


	return render_template("input.html", trees = trees, graphs = graphs)

@app.route("/planar_maps")
def planar_maps():
	trees = getAllTreeReps(3)
	print(len(trees))
	allGraphs = []
	allStrings = []

	if platform == "linux" or platform == "linux2":
		prog = "skew.out"
	elif platform == "win32":
		prog = "skew.exe"
	else:
		return render_template("error.html", error="no suitable os!")
	program = os.path.join(os.path.dirname(__file__), prog)

	for tree in trees:
		graphs = []
		strings = []

		output, error = process.runProccess(program, tree)
		ddpDict, _ = extract.extractProcessOutput(output)

		for ddp in ddpDict.values():
			nodes, edges = planarMap(ddp)

			graphs.append({ 'nodes': nodes, 'edges': edges })
			strings.append("({})".format(")(".join(["".join(x).lower() for x in ddp])))

		allGraphs.append(graphs)
		allStrings.append(strings)

	return render_template("planar_maps.html", graphs=allGraphs, strings=allStrings)

@app.route("/ternary_trees")
def ternary_trees():
	trees = getAllTreeReps(3)
	print(len(trees))
	allGraphs = []
	allStrings = []

	if platform == "linux" or platform == "linux2":
		prog = "skew.out"
	elif platform == "win32":
		prog = "skew.exe"
	else:
		return render_template("error.html", error="no suitable os!")
	program = os.path.join(os.path.dirname(__file__), prog)

	for tree in trees:
		trees = []
		strings = []

		output, error = process.runProccess(program, tree)
		_, treeDict = extract.extractProcessOutput(output)

		trees.append(Tree(tree, x_shift = 0.5).getData())
		strings.append("[{}]".format(" ".join(["".join(x).upper() for x in tree])))

		for t in treeDict.values():
			trees.append(Tree(t, x_shift = 0.5).getData())
			strings.append("[{}]".format(" ".join(["".join(x).upper() for x in t])))

		allGraphs.append(trees)
		allStrings.append(strings)

	return render_template("ternary_trees.html", graphs=allGraphs, strings=allStrings)



def getGraph(ddpDict):
	nodes, edges = planarMap(ddpDict)
	name = "({})".format(")(".join(["".join(x).lower() for x in ddpDict]))
	return { 'nodes': nodes, 'edges': edges, 'string': name }

if __name__ == "__main__":
   app.run("0.0.0.0", 5010, debug=True)
