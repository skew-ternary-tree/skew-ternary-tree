from collections import defaultdict

def extractProcessOutput(lines):
    ddpDict = defaultdict(list)
    treeDict = defaultdict(list)
    capture = 0
    ddpNum = 0
    treeNum = 0

    for line in lines:
        # find the line that contains the tree information
        # needed for ....
        if line.startswith("+"):
            # increase number
            treeNum = treeNum + 1
            # since no string.replace does not accept regex first
            # replace the + sign and then then :.
            treeNode = line.replace("+", "").replace(":", "").strip().split(" ")

            for node in treeNode:
                treeDict[treeNum].append(node)

        # find the ddp representation and capture the arguments
        if line.startswith("--- DDP"):
            ddpNum = ddpNum + 1
            capture = 1
            continue

        # ignore line
        if line.startswith("Vertices:"):
            continue

        # ignore line
        if line.startswith("Faces:"):
            continue

        # marks the end of a ddp (also the js representation, but we ignore that one)
        if line.startswith("(Altogether") and capture == 1:
            capture = 0

        # capture all lines between start and end mark
        if capture:
            ddp = line.strip().replace("*", "R").split(" ")
            ddpDict[ddpNum].append(ddp)

    return ddpDict, treeDict
