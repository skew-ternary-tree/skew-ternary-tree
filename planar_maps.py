import math
def getEdges(nodes):
	edges = {}
	for key, value in nodes.items():
		for edge in value:
			if edge not in edges:
				edges[edge] = (key,)
			else: 
				edges[edge] += (key,)
	
	return edges
	
def planarMap(args):	
	nodes = []
	faces = []
	edges = set()

	# Finde alle Knoten und Flächen, sowie Kanten
	for arg in args:
		if "0" in arg[0] or "2" in arg[0]:
			nodes.append([x[0].lower() for x in arg])
			edges.update(nodes[-1])
		else:
			faces.append([x[0].lower() for x in arg])

	multiEdges = {} # Zum finden aller doppelten Kanten
	circle = [] # Der äußere Kreis
	edge = 'r' # Wir fangen mit der Root-Edge an
	current = 0
	# Finden des äußeren Kreises.
	while current != 0 or not circle:
		circle.append(current)
		edge_index = nodes[current].index(edge)
		# Finde das target der current-edge
		nodes[current] = nodes[current][edge_index:] + nodes[current][:edge_index]
		# Das target ist der neue node
		new_node = next(i for i, n in enumerate(nodes) if edge in n and i != current)
		# Füge die gefundene Kante zu Multiedges hinzu
		a = min(new_node, current)
		b = max(new_node, current)
		if (a, b) not in multiEdges:
			multiEdges[(a, b)] = [edge]
		elif edge not in multiEdges[(a, b)]:
			multiEdges[(a, b)] += [edge]
		current = new_node
		# Finde die nächste Kante vom neuen Knoten aus (gegen den Uhrzeigersinn um den Knoten herum)
		edge = nodes[current][(nodes[current].index(edge) - 1) % len(nodes[current])]

	nodesWithCoord = {}
	normals = {}

	# Der Mittelpunkt des Graphen. Falls der äußere Kreis ein Halbkreis ist, brauchen wir einen anderen Mittelpunkt.
	mid = (1, 1)
	if len(circle) == 2:
		mid = (1, 1.5)

	# Winkel um die Knoten auf dem äußeren Kreis anzuordnen
	delta = 2 * math.pi / len(circle)
	alpha = 0.5 * (-math.pi + delta)
	for k, node in enumerate(circle):
		_nodes = []
		# Finde alle Knoten die von den äußeren Knoten erreichbar sind und ihre Kanten
		for i in range(len(nodes[node])-1, 0, -1):
			new_node = next(j for j, n in enumerate(nodes) if nodes[node][i] in n and j != node)
			if new_node not in circle and new_node not in _nodes:
				_nodes.append(new_node)
			a = min(new_node, node)
			b = max(new_node, node)
			if (a, b) not in multiEdges:
				multiEdges[(a, b)] = [nodes[node][i]]
			elif nodes[node][i] not in multiEdges[(a, b)]:
				for multiEdge in multiEdges[(a, b)]:
					if multiEdge in normals:
						normals[nodes[node][i]] = normals[multiEdge]
				multiEdges[(a, b)] += [nodes[node][i]]

		# Platziere den äußeren Knoten gleichmäßig im Kreis
		x = 1 + 1 * math.cos(alpha)
		y = 1 + 1 * math.sin(alpha)
		nodesWithCoord[node] = (x, y)


		# Setze die vorherige Kante auf dem äußeren Kreis als Normale für die jetzige Kante
		id = nodes[circle[(k + 1) % len(circle)]][0]
		normals[id] = (x, y)

		if id == 'r':
			for multiEdge in multiEdges:
				if id in multiEdges[multiEdge]:
					for mEdge in multiEdges[multiEdge]:
						normals[mEdge] = (x, y)

		# Platziere alle Knoten die von dem äußeren Knoten erreichbar sind relativ zum äußeren Knoten
		if len(_nodes) != 0:
			_delta = 0.25 * math.pi / len(_nodes)
			norm = math.sqrt((mid[0] - x)**2 + (mid[1] - y)**2)
			_alpha = (-1 if y > mid[1] else 1) * math.acos((mid[0] - x) / norm)
			_alpha = _alpha + _delta * (len(_nodes) - 1) / 2
			for _node in _nodes:
				if _node not in nodesWithCoord:
					nodesWithCoord[_node] = (x + 0.75 * norm * math.cos(_alpha), y + 0.75 * norm * math.sin(_alpha))
					_alpha = _alpha - _delta
		alpha = alpha - delta

	# Platziere alle Knoten, die dadurch nicht gefunden wurden in der Mitte (keine richtige Lösung)
	for i in range(len(nodes)):
		if i not in nodesWithCoord:
			nodesWithCoord[i] = mid

	edges = getEdges({i: nodes[i] for i in nodesWithCoord})

	# Erstelle die Dictionary Einträge der Kanten
	edges = [{
		'id': i, 
		'label': str(i),
		'source': x[0],
		'target': x[1], 
		'size': 1,
		'type': 'def',
		'color': '#000000',
		'count': 0
	} for i, x in edges.items() if len(x) == 2]

	# Finde alle Doppelkanten und bestimme ihre Art der Bögen
	for key, value in multiEdges.items():
		if len(value) > 1:
			for i, edge in enumerate(value):
				for e in edges:
					if e['id'] == edge:
						if len(value) > 2:
							if i == 0:
								e['type'] = 'curve'
							
							# Mehrere Doppelkanten parallel zur Root-Edge
							elif i == 3 and 'r' in value and len(circle) == 2:
								e['type'] = 'curve'
								e['source'], e['target'] = e['target'], e['source']
							elif i == 2 and (not 'r' in value or len(circle) > 2):
								e['type'] = 'curve'
								e['source'], e['target'] = e['target'], e['source']
						else:
							if i > 0 and not ('r' in value and len(circle) == 2):
								e['type'] = 'curve'
								if e['id'] in normals:
									# Bestimmung in welche Richtung sich die Kante biegt
									normal = normals[e['id']]
									x1 = nodesWithCoord[e['source']][0]
									x2 = nodesWithCoord[e['target']][0]
									y1 = nodesWithCoord[e['source']][1]
									y2 = nodesWithCoord[e['target']][1]

									cp = ((x1 + x2) / 2 + (y2 - y1) / 4, (y1 + y2) / 2 + (x1 - x2) / 4)
									
									dx1 = normal[0] - x1
									dy1 = normal[1] - y1
									dx2 = cp[0] - x1
									dy2 = cp[1] - y1
									# Bestimmung ob die Biegung falsch ist.
									if (dx1 * dx2 + dy1 * dy2) /( math.sqrt(dx1**2 + dy1**2) * math.sqrt(dx2**2 + dy2**2)) > 0:
										# Tausch von source und target, falls ja.
										e['source'], e['target'] = e['target'], e['source']

						break

	# Nur zwei Knoten auf dem äußeren Kreis -> Halbkreis
	if len(circle) == 2:
		edges[1]['type'] = 'arc'

	# Dictionary der Knoten.
	nodes = [{
		'id': i,
		'label': str(i),
		'x': x,
		'y': -y,
		'size': 1,
		'type': 'circle',
		'color': '#ffffff',
		'borderColor': '#ff0000'
	} for i, (x, y) in nodesWithCoord.items()]
				
	return nodes, edges
	