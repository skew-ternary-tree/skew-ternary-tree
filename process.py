import subprocess

def runProccess(program, args):
    program_input = []
    program_output = []
    program_error = []

    program_input.append(program)

    # add all arguments to input
    for arg in args:
        program_input.append(arg)

    output = []
    error = []

    # run process
    proc = subprocess.Popen(program_input,
                            shell=True,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            )

    # program pipes
    output_pipe = proc.stdout
    error_pipe = proc.stderr

    for line in error_pipe:
        line = line.decode('utf8')
        line = str(line).strip()
        program_error.append(line)

    # save output as list
    for line in output_pipe:
            line = line.decode('utf8')
            line = str(line).strip()
            program_output.append(line)

    return program_output, program_error
