// allowed tree letters
var availChars = 'abcdefghijklmnopqstuvwxyz'.toUpperCase().split('');
var returnedChars = [];
var index = 0;

// resets the letters + index
function reset() {
    availChars = 'abcdefghijklmnopqstuvwxyz'.toUpperCase().split('');
    returnedChars = [];
    index = 0;
}

// gets the current id
function getId() {
    return index++;
}

// gets the current available letter
// or -1 is no letter are available
function getAvailLetter() {
    if (returnedChars.length > 0) {
        return returnedChars.shift();
    }

    if (availChars.length > 0) {
        return availChars.shift();
    }
    return null;
}

// remove a letter from the available letters
// used to build the tree from a string
function removeLetter(letter) {
    var index = availChars.indexOf(letter);
    availChars.splice(index, 1);
}

// if a tree node is removed
// we can reclaim that letter
function returnLetter(letter) {
    returnedChars.push(letter);
    returnedChars.sort();
}

// adds a node to the graph
function addNode(graph, id, label, x, y, size, active, rank, type, color) {
	color = color || '#000000';
    graph.addNode({
        id: id,
        label: label,
        x: x,
        y: y,
        size: size,
		active: active,
		color: color,
        rank: rank,
        type: type
    });
}
// adds an edge between two nodes.
function addEdge(graph, source, target) {
    graph.addEdge({
        id: source + "-" + target,
        source: source,
        target: target
    });
}

function toggleNode(node, sigma, graph) {
    if(isPrimitive(node)) {
        throw "not a Node!"
    }
    if(!node.root) {
        if(node.active) {
            node.active = 0;
            node.type = "star";
            returnLetter(node.label);
            node.label = "";
            var nodes = getAllNodesRelated(node);

            Object.entries(nodes).forEach(([key, value]) => {
                if (value.label != "") {
                    returnLetter(value.label);
                }
                graph.dropNode(value.id);
            });

            sigma.refresh();
            treeLayout(graph);
            sigma.refresh();
            return;
        }

        if(!node.active && node.rank >= 0) {
            var letter = getAvailLetter();
            if(letter == null) {
                return;
            }

            node.active = 1;
            node.label = letter;
            node.type = "circle";

            var index1 = getId();
            var index2 = getId();
            var index3 = getId();

            addNode(graph, index1, "", node.x - 1, node.y + 1, 1, 0, node.rank - 1, "star", node.rank == 0 ? '#838282' : '#000000');
            addEdge(graph, node.id, index1);

            addNode(graph, index2, "", node.x, node.y + 1, 1, 0, node.rank, "star");
            addEdge(graph, node.id, index2);

            addNode(graph, index3, "", node.x + 1, node.y + 1, 1, 0, node.rank + 1, "star");
            addEdge(graph, node.id, index3);

            sigma.refresh();
            treeLayout(graph);
            sigma.refresh();
        }
    }
}

// gets all nodes related to one single node
function getAllNodesRelated(node) {
    var items = [];
    items.push(...getNodesRelated(node));
    return items;
}

function getNodesRelated(node) {
    var item = [];
    Object.entries(s.graph.getOutNeighborNodes(node)).forEach(([key, val]) => {
        item.push(...getNodesRelated(val));
        item.push(val);
    });

    return item;
}

// gets the string representation of a graph.
function graphToString(graph) {
    var nodeArrayString = [];
    var activeNodes = [];

    Object.entries(graph.nodes()).forEach(([key, val]) => {
        if(val.active) activeNodes.push(val);
    });

    Object.entries(activeNodes).forEach(([key, val]) => {
        var string = val.label;

        Object.entries(graph.getOutNeighborNodes(val)).forEach(([k, v]) => {
            if(v.active) {
                string = string + v.label;
            } else {
                string = string + "-";
        }});

    nodeArrayString.push(string);
    });

    return nodeArrayString;
}

// checks if the node string is vaild
function checkNodes(node) {
    var letter = [];

    for (item in node) {
        var str = node[item];
        for (var i = 0; i < str.length; i++) {
            var c = str.charAt(i);
            if (c != "-") {
                if (letter.indexOf(c) == -1) {
                    letter.push(c);
                }
            }
        }
    }

    for (item in node) {
        var first = node[item].charAt(0);
        var index = letter.indexOf(first);
        if (index == -1) return null;

        letter.splice(index, 1);
    }
    if (letter.length > 0) return null;

    return 1;
}

// builds a graph from a string
function buildGraphFromString(string, graph, sigma) {
    var testRegex = /^([A-Z-]{4})( [A-Z-]{4})*$/;
    if (string.match(testRegex) == null) {
        throw "Invalid graph";
    }

    var splits = string.split(" ");
    if (checkNodes(splits) == null) {
        throw "Invalid graph";
    }

    graph.clear();
    reset();
    sigma.refresh();

    var x = 0;
    var y = 0;
    var rank = 0;

    var root = splits[0].charAt(0);
    var parentId = getId();

    removeLetter(root);
    addNode(graph, parentId, root, x, y, 1, 1, 0, "circle");
    y++;
    s.refresh();
    for (item in splits) {
        var itemString = splits[item];
        rank = -1;
        x = -1;
        var parentNode = graph.getNodeByName(splits[item].charAt(0));
        parentId = parentNode.id;
        rank = parentNode.rank - 1;
        for (var i = 0; i < itemString.length; i++) {
            var c = itemString.charAt(i);
            if (c == "-") {
				var id = getId();
                addNode(graph, id, "", x++, y, 1, 0, rank++, "star", rank == 0 ? '#838282' : '#000000');
                addEdge(graph, parentId, id);
                sigma.refresh();

            } else {
                if (c == root) {
                    continue;
                }
                if (s.graph.getNodeByName(c) == null) {
                    var id = getId();
                    removeLetter(c);
                    if (rank == -1) {
                        graph.clear();
                        sigma.refresh();
                        reset();
                        throw "illegal rank!"
                    }
                    addNode(graph, id, c, x++, y, 1, 1, rank++, "circle");
                    addEdge(graph, parentId, id);
                    sigma.refresh();
                }
            }
        }
        y++;
    }

    treeLayout(graph);
    sigma.refresh();
}