//Implemntation von http://dirk.jivas.de/papers/buchheim02improving.pdf

    // set the distance between nodes to 1
	var distance = 1;
	
	// is a primtive type?
	function isPrimitive(val) {
		return (val !== Object(val));
	}
	
	// number of outgoing neighbors
	sigma.classes.graph.addMethod('getOutNeighborCount', function(node) {
		if(isPrimitive(node)) throw "not a node!";
		
		var inode = this.getNodeById(node.id);
		
		return Object.keys(this.outNeighborsIndex[inode.id]).length;
	});
	
	// gets a node by the id specifier
	sigma.classes.graph.addMethod('getNodeById', function(id) {
		for(var node in this.nodesArray) {
			if(this.nodesArray[node].id == id) {
				return this.nodesArray[node];
			}
		}
		return null;
	});
	
	// get the outgoing neighbors
	sigma.classes.graph.addMethod('getOutNeighborNodes', function(node) {
		if(isPrimitive(node)) throw "not a node!";
		
		var inode = this.getNodeById(node.id);
		
		if(inode == null) return null;
				
		var edges = this.outNeighborsIndex[inode.id];
		
		var nodes = [];
		
		var fnode = null;
		
		Object.entries(edges).forEach(([key, val]) => {
			Object.entries(val).forEach(([k, v]) => {
				fnode = this.getNodeById(v.target);
				if(fnode != null) nodes.push(fnode);
			});
		});

		return nodes;
	});

	// get a node by the name specifier
	sigma.classes.graph.addMethod('getNodeByName', function(name) {
		for (var node in this.nodesArray) {
			if(this.nodesArray[node].label == name) {
				return this.nodesArray[node];
			}
		}
		return null;
	});
	
	// get the parent node
	sigma.classes.graph.addMethod('getParent', function(node) {
		if(isPrimitive(node)) throw "node a node!"; 
		
		var inode = this.getNodeById(node.id);
		
		var item = this.inNeighborsIndex[inode.id];
		if(item == null) return null;
		
		var parentNode = null;
		
		var fnode = null;
		Object.entries(item).forEach(([key, val]) => {
			Object.entries(val).forEach(([k, v]) => {
				fnode = this.getNodeById(v.source);
				if(fnode != null) parentNode = fnode;
			});
		});
		return parentNode;
	});
	
    // parse an array
	function reverse(obj) {
		var new_obj = {};
		
		var keys = Object.keys(obj);
		var values = Object.values(obj);
		var j = 1;
		for(var i = keys.length-1; i >= 0; i--) {
			new_obj[j] = values[i];
			j++;
		}
		return new_obj;
	}
	
	// get the next left node
	function nextLeft(graph, node_v) {
		//console.log("nextLeft(node_v=",node_v,")");
		if (graph.getOutNeighborCount(node_v) > 0) {
			return getFirstElement(graph.getOutNeighborNodes(node_v));
		} else {
			return node_v._thread;
		}
	}
	
	// get the next right node
	function nextRight(graph, node_v) {	
		//console.log("nextRight(node_v=",node_v,")");
		if(graph.getOutNeighborCount(node_v) > 0) {
			return getLastElement(graph.getOutNeighborNodes(node_v));
		} else {
			return node_v._thread;
		}
	}
	
	// get the left most sibling node
	function leftMostSibling(graph, node_v) {
		//console.log("leftMostSibling(node_v=", node_v,")");
		if(node_v == null) return null;
		
		var parentNode = graph.getParent(node_v);
		
		if(parentNode == null) return null;	
		if(graph.getOutNeighborCount(parentNode) == 0) return null;

		return getFirstElement(graph.getOutNeighborNodes(parentNode));
		
	}
	
	// get the left sibling node
	function leftSibling(graph, node_v) {
		//console.log("leftSibling(node_v=", node_v,")");
		if(node_v == null) return null;
		
		var found = false;
		var tempNode = node_v;
		parentNode = graph.getParent(node_v);
		if(parentNode == null)  return null;	
		
		if(graph.getOutNeighborCount(parentNode) == 0) return null;
		
		Object.entries(graph.getOutNeighborNodes(parentNode)).forEach(([key, val]) => {
			if(val == node_v) {
				found=true;
			}
			if(!found) {
				tempNode = val;
			}
		});
		if(found) {
			return tempNode;
		}
		return null;
	}
	
	// move the subtree
	function moveSubtree(node_wm, node_wp, shift) {
		//console.log("moveSubtree(node_wm=",node_wm,", node_wp=",node_wp,", shift=",shift,")");
		var subtrees = node_wp._number - node_wm._number;
		node_wp._change = node_wp._change - (shift / subtrees)
		node_wp._shift = node_wp._shift + shift;
		node_wm._change = node_wm._change + (shift / subtrees)
		node_wp._prelim = node_wp._prelim + shift;
		node_wp._mod = node_wp._mod + shift;		
	}
	
	// perform shifts
	function executeShifts(graph, node_v) {
		//console.log("executeShifts(node_v=",node_v,")");
		if(node_v.active == 0) 	return;
		
		if(graph.getOutNeighborCount(node_v) == 0) return;

		var shift = 0;
		var change = 0;
			
		Object.entries(reverse(graph.getOutNeighborNodes(node_v))).forEach(([key, val]) => {
			val._prelim = val._prelim + shift;
			val._mod = val._mod + shift;
			change = change + val._change;
			shift = shift + val._shift + change;
		});	
	}
	
	
	// get the ancestor node
	function getAncestor(graph, node_vI, node_v) {
		//console.log("getAncestor(node_vI=",node_vI,", node_v=",node_v,")");
		var ancestor = node_vI._ancestor;
		if(graph.getParent(ancestor) == graph.getParent(node_v)) return ancestor;
		return null;
	}
	
	// perform the second walk
	function secondWalk(graph, node_v, m) {
		//console.log("secondWalk(node_v=", node_v, ",m=", m,")");
		node_v.x = node_v._prelim + m;

		Object.entries(graph.getOutNeighborNodes(node_v)).forEach(([key, val]) => {
			secondWalk(graph, val, m + node_v._mod);
		});
	}
	
	// apport
	function apportion(graph, node_v, defaultAncestor) {
		//console.log("apportion(node_v.id=",node_v,"defaultAncestor=",defaultAncestor,")");
		var node_w = leftSibling(graph, node_v);		
		if(node_w == null) return defaultAncestor; 
		
		var node_vip = node_v;
		var node_v0p = node_v;
		var node_vim = node_w;
		var node_v0m = leftMostSibling(graph, node_vip);
	
		
		var sip = node_vip._mod;
		var s0p = node_v0p._mod;
		var sim = node_vim._mod;
		var s0m = node_v0m._mod;
			
		while((nextRight(graph, node_vim) != null) && (nextLeft(graph, node_vip) != null)) {
			node_vim = nextRight(graph, node_vim);
			node_vip = nextLeft(graph, node_vip);
			node_v0m = nextLeft(graph, node_v0m);
			node_v0p = nextRight(graph, node_v0p);

			node_v0p._ancestor = node_v;
			
			var shift = (node_vim._prelim + sim) - (node_vip._prelim + sip) + distance;
			if(shift > 0) {
				var ancestor = getAncestor(graph, node_vim, node_v);
				if(ancestor == null) {
					ancestor = defaultAncestor;
				}
				moveSubtree(ancestor, node_v, shift);
				sip = sip + shift;
				s0p = s0p + shift;
			}
			
			sim = sim + node_vim._mod;
			sip = sip + node_vip._mod;
			s0m = s0m + node_v0m._mod;
			s0p = s0p + node_v0p._mod;
		}
		
		if(nextRight(graph, node_vim) != null && nextRight(graph, node_v0p) == null) {
			node_v0p._thread = nextRight(graph, node_vim);
			node_v0p._mod = node_v0p._mod + (sim - s0p);
		}
		
		if(nextLeft(graph, node_vip) != null && nextLeft(graph, node_v0m) == null) {
			node_v0m._thread = nextLeft(graph, node_vip);
			node_v0m._mod = node_v0m._mod + (sip - s0m);
			defaultAncestor = node_v;
		}
		
		return defaultAncestor;			
	}
	
	// get the first element in an array
	function getFirstElement(arr) {
		return Object.values(arr).slice().shift();
	}
	
	// get the last element in an array
	function getLastElement(arr) {
		return Object.values(arr).slice(-1).pop();
	}
	
	// perform the first walk
	function firstWalk(graph, node_v) {
		var node_w;
		if(node_v.active == 0) {
			node_w = leftSibling(graph, node_v);
			
			if(node_w != null) {
				node_v._prelim = node_w._prelim + distance;
			}
			
		} else {
			var defaultAncestor = getFirstElement(graph.getOutNeighborNodes(node_v));
			Object.entries(graph.getOutNeighborNodes(node_v)).forEach(([key, val]) => {
				firstWalk(graph, val);
				defaultAncestor = apportion(graph, val, defaultAncestor);
			});

			executeShifts(graph, node_v);
			var mid_point = (nextLeft(graph, node_v)._prelim + nextRight(graph, node_v)._prelim) / 2;
			
			node_w = leftSibling(graph, node_v);
			
			if(node_w != null) {
				node_v._prelim = node_w._prelim + distance;
				node_v._mod = node_v._prelim - mid_point;
			}
			else {
				node_v._prelim = mid_point;
			}
		}
	}
	
	// main method to run the algorithm
	function treeLayout(graph) {
		//console.log(graph.nodes());
		Object.entries(graph.nodes()).forEach(([key, val]) => {
			val._mod = 0;
			val._thread = null;
			val._ancestor = val;
			val._change = 0;
			val._shift = 0;
			val._prelim = 0;
			
			if(graph.getOutNeighborCount(val) > 0) {
				var i = 0;
				Object.entries(graph.getOutNeighborNodes(val)).forEach(([k, v]) => {
					v._number = i;
					i++;
				});
			}
		});
		
		var node_r = graph.nodes(0);
		
		firstWalk(graph, node_r);
		secondWalk(graph, node_r, -node_r._prelim);
	}