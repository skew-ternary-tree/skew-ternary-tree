import string

class Node:
	def __init__(self, rank):
		self.rank = rank
		self.children = [None, None, None]

	def copy(self):
		node =  Node(self.rank)
		node.children = [x.copy() if x is not None else None for x in self.children]
		return node
		
	def toString(self, char, remainingChars):
		strings = [char]
		for child in self.children:
			if child is None:
				strings[0] += '-'
			else:
				char = remainingChars[0]
				strings[0] += char
				del remainingChars[0]
				strings += child.toString(char, remainingChars)
		return strings

def getAllTrees(root, remainingDepth):
	remainingDepth = remainingDepth - 1
	if remainingDepth < 1:
		return [root]

	leftTrees = [None]
	if root.rank > 0:
		leftTrees += getAllTrees(Node(root.rank - 1), remainingDepth)
	
	midTrees = [None] + getAllTrees(Node(root.rank), remainingDepth)
	rightTrees = [None] + getAllTrees(Node(root.rank + 1), remainingDepth)

	trees = []
	for lt in leftTrees:
		for mt in midTrees:
			for rt in rightTrees:
				root.children = [lt, mt, rt]
				trees.append(root.copy())
	return trees

def getAllTreeReps(depth):
	trees = getAllTrees(Node(0), depth)
	reps = []
	for tree in trees:
		reps.append(tree.toString('A', list(string.ascii_uppercase)[1:]))
	return reps

if __name__ == "__main__":
	trees = getAllTrees(Node(0), 3)
	for tree in trees:
		print(' '.join(tree.toString('A', list(string.ascii_uppercase)[1:])))