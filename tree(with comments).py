import math

class Node:
	
	def __init__(self, id, name):
		self.id = id
		self.name = name
		self.nodes = [None, None, None]
		self.parent = None
		
		self.rel_x = 0
		self.rel_y = 0
		
	def getDict(self): 
		"""determines how a node is presented;
		   input for the visualising modul"""
		return {
			'id': self.id,
			'label': self.name,
			'x': self.getAbsX(),
			'y': self.getAbsY(),
			'size': 1,
			'type': 'circle',
			'color': '#ffffff',
			'borderColor': '#000000'
		}
	
	# Pre-Order
	def getNodes(self): 
		"""Returns a list of the nodes children"""
		list = [self]
		for node in self.nodes:
			if node is not None:
				list.extend(node.getNodes())
		return list
		
	def getAbsX(self):
		"""returns the final x-coordinate of the node"""
		if self.parent is None:
			return self.rel_x
		else:
			return self.rel_x + self.parent.getAbsX()
			
	def getAbsY(self):
		"""returns the final y-coordinate of the node"""
		if self.parent is None:
			return self.rel_y
		else:
			return self.rel_y + self.parent.getAbsY()
		
			
		
class Tree:

	def __init__(self, args, x_shift = 1, y_shift = 1, name="T"):  
		self.name = name
		self.x_shift = x_shift
		self.y_shift = y_shift
		
		self.root = self.parse(args)
		self.span = self.buildTree(self.root)
		
	def parse(self, nodes):
		"""
		
		parses the input and creates nodes
		
		Parameters
		----------
		nodes : list of the nodes information ; contains strings,each consisting out of four characters,
				first character: parent
				following characters: children; 
				the positions of the children left,middle,right represent the orientations towards the parent
		Returns
		-------
		type: Node , the root of the tree
		"""
		nodesByName = {}
		for node in nodes:
			nodesByName[node[0]] = Node(len(nodesByName), node[0])
			
		for node in nodes:
			current = nodesByName[node[0]]			
			for i in range(1, 4):
				if node[i] != '-':
					if node[i] not in nodesByName:
						child = Node(len(nodesByName), node[i])
						nodesByName[node[i]] = child
					else:
						child = nodesByName[node[i]]
					child.parent = current
					current.nodes[i - 1] = child
		
		for node in nodesByName.values():
			if node.parent is None:
				return node
	
	def buildTree(self, node):
	"""
	calculates recursively the coordinates of the nodes in the tree
	
	Parameters
	----------
	type:Node ,the root of the tree
	
	Returns
	-------
	a tupel with two integers; they stand for the x-axis shift of 
	other nodes/subtrees to the current tree:(<shift left>, <shift right>)
	"""
		if node is None:
			return (0, 0)
	
		span = [(0, 0), (0, 0), (0, 0)]
		shift = [-self.x_shift, 0, self.x_shift]
		
		for i in range(3):
			if node.nodes[i] is not None:
				span[i] = self.buildTree(node.nodes[i])
				
		shift[0] -= span[0][1] - span[1][0]
		shift[1] = 0
		shift[2] += span[1][1] - span[2][0]
		
		for i in range(3):
			if node.nodes[i] is not None:
				node.nodes[i].rel_x = shift[i]
				node.nodes[i].rel_y = self.y_shift
		
		return (span[0][0] + shift[0], span[2][1] + shift[2])
	
	def getData(self):
		"""delivering the input for the visualising modul"""
		nodes = self.root.getNodes() if self.root is not None else []
			
		sigmaNodes = []
		sigmaEdges = []
		for node in nodes:
			sigmaNodes.append(node.getDict())
			if node.parent is not None:
				sigmaEdges.append({
					'id': len(sigmaEdges),
					'source': node.parent.id,
					'target': node.id,
					'size': 1,
					'color': '#000000'
				})
			
		return { 'nodes' : sigmaNodes, 'edges' : sigmaEdges, 'string' : self.name }
	
		
			
			
